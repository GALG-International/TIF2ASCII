


#include "TIFF2ASCII.h"

Tiff2Ascii::Tiff2Ascii() = default;

Tiff2Ascii::~Tiff2Ascii() = default;

std::string Tiff2Ascii::returnFileName(const std::string &fileAdd, const std::string &extension) {
    // find file name without extension and add extension like (.tiff or .asc)

    std::filesystem::path fileName_ = std::filesystem::path(fileAdd).filename();
    std::string fileName{fileName_.u8string()};

    std::string::size_type const p(fileName.find_last_of('.'));
    std::string fileWithout_extension = fileName.substr(0, p);
    std::string filename = fileWithout_extension + extension;

    return filename;
}



std::vector<std::vector<float>> Tiff2Ascii::Tif2Vector(const std::string &fileAdd, const bool &saveOrNot) {

    try {

        // read tiff file
        // check to see if gdal in initialized or not, if not initialize it
        if (!_isGDALinitialized) {
            // gdal initialization
            GDALAllRegister();
            GDALDriverManager::AutoLoadDrivers();
            _isGDALinitialized = true;
        };

        auto *tiff = (GDALDataset *) GDALOpen(fileAdd.data(), GA_ReadOnly);

        if (tiff == NULL) {
            std::cout << "Failed to open the fila" << std::endl;
        }

        // gat the raster band
        GDALRasterBand *tiffBand = tiff->GetRasterBand(1);

        // read data into memory
        int width = tiffBand->GetXSize();
        int height = tiffBand->GetYSize();

        std::cout << width << ",  " << height << std::endl;

        float *tiffData = new float[width * height];
        tiffBand->RasterIO(GF_Read, 0, 0, width, height, tiffData, width, height, GDT_Float32, 0, 0);
        // create vector of vector
        std::vector<std::vector<float>> data(height, std::vector<float>(width));

        if (saveOrNot) {
            // save file in same add as .asc file
            std::string filename = returnFileName(fileAdd, ".asc");

            // create ascii file
            FILE *ascii = fopen(filename.data(), "w");

            // adding header files
            fprintf(ascii, "[Settings]\n"
                           "Version=3\n"
                           "ImageWidth=640\n"
                           "ImageHeight=512\n"
                           "ShotRange=16.85;36.85\n"
                           "CalibRange=0.00;50.00\n"
                           "TempUnit=°C\n"
                           "\n"
                           "[Data]\n");

            // adding data
            for (int ii = 0; ii < height; ii++) {
                for (int jj = 0; jj < width; jj++) {
                    data[ii][jj] = tiffData[ii * width + jj];
                    fprintf(ascii, "%f", tiffData[ii * width + jj]);
                    fprintf(ascii, "\t");
                }
                fprintf(ascii, "\n");
            }
            fclose(ascii);

        } else {
            // just creating the vector of vector without saving it
            for (int ii = 0; ii < height; ii++) {
                for (int jj = 0; jj < width; jj++) {
                    data[ii][jj] = tiffData[ii * width + jj];
                }
            }
        }

        delete[] tiffData;
        GDALClose(tiff);
        return data;
    } catch (const char *e) {
        std::cout << e << std::endl;
    }
}

void Tiff2Ascii::saveAsGrayScale(const std::string &fileAdd, const std::string &saveDir){

    if (!_isGDALinitialized) {
        // gdal initialization
        GDALAllRegister();
        GDALDriverManager::AutoLoadDrivers();
        _isGDALinitialized = true;
    };


    GDALDataset* dataset = (GDALDataset*) GDALOpen(fileAdd.data(), GA_ReadOnly);
    int width = dataset->GetRasterXSize();
    int height = dataset->GetRasterYSize();

    float * inputdata = new float[width * height];

    dataset->RasterIO(GF_Read, 0, 0, width, height, inputdata, width, height, GDT_Float32, 1, nullptr, 0, 0, 0);
    GDALClose(dataset);

    cv::Mat  grayImg(height, width, 1);

    for (int y = 0; y < height ; y++){
        for(int x = 0; x< width; x++){
            grayImg.at<uchar>(y , x) = static_cast<uchar>(inputdata[y * width + x]);
        }
    }
    std::string filename = returnFileName(fileAdd, ".tif");


    std::cout << saveDir+filename << std::endl;

//    grayImg.convertTo(grayImg, CV_8U);
//    cv::Mat dst;
//    cv::equalizeHist(grayImg, dst);

    cv::imwrite(saveDir+filename, grayImg);
    delete[] inputdata;
//    return 0;

}

void Tiff2Ascii::changeAndSave_oneTiffFile(const std::string &fileAdd, const std::string &saveDir) {

    try {

        // read tiff file
        if (!_isGDALinitialized) {
            // gdal initialization
            GDALAllRegister();
            GDALDriverManager::AutoLoadDrivers();
            _isGDALinitialized = true;
        };

        auto *tiff = (GDALDataset *) GDALOpen(fileAdd.data(), GA_ReadOnly);

        if (tiff == NULL) {
            std::cout << "Failed to open the fila" << std::endl;
        }

        // gat the raster band
        GDALRasterBand *tiffBand = tiff->GetRasterBand(1);

        // read data into memory
        int width = tiffBand->GetXSize();
        int height = tiffBand->GetYSize();

        std::cout << "the size of image is: " << width << ",  " << height << std::endl;

        float *tiffData = new float[width * height];
        tiffBand->RasterIO(GF_Read, 0, 0, width, height, tiffData, width, height, GDT_Float32, 0, 0);


        std::string filename = returnFileName(fileAdd, ".asc");

        /// stiching Save directory and file
        std::string newSaveFileDir = saveDir+ filename;



        FILE *ascii = std::fopen(newSaveFileDir.data(), "w");

        // adding header files
        fprintf(ascii, "[Settings]\n"
                       "Version=3\n"
                       "ImageWidth=640\n"
                       "ImageHeight=512\n"
                       "ShotRange=16.85;36.85\n"
                       "CalibRange=0.00;50.00\n"
                       "TempUnit=°C\n"
                       "\n"
                       "[Data]\n");

        // adding data
        for (int ii = 0; ii < height; ii++) {
            for (int jj = 0; jj < width; jj++) {
                fprintf(ascii, "%f", tiffData[ii * width + jj]);
                fprintf(ascii, "\t");
            }
            fprintf(ascii, "\n");
        }

        fclose(ascii);
        delete[] tiffData;
        GDALClose(tiff);
    } catch (const char *e) {
        std::cout << e << std::endl;
    }

}



void Tiff2Ascii::changeAndSave_MultipleTifFile(const std::string &tiffFilesDir, const std::string &saveDir) {
    try {

        // get the tiff name and save ita as ascii file in provided directory
        for (const auto &entry: std::filesystem::directory_iterator(tiffFilesDir)) {
            if (entry.path().extension() == ".tiff" || entry.path().extension() == ".tif") {
                std::string fileDir = entry.path();
                std::cout << entry.path().filename() << " \n" << std::endl;

                // save file in new folder
                changeAndSave_oneTiffFile(fileDir.data(), saveDir);
            }
        }
    } catch (const char *e) {
        std::cout << e << std::endl;
    }

}



void Tiff2Ascii::tif_to_mat_multiple(const std::string &tiffFilesDir, const std::string &saveDir) {
    try {
        // create new folder in given directory
        //removeCreateDir(saveDir);

        // get the tiff name and save ita as ascii file in provided directory
        for (const auto &entry: std::filesystem::directory_iterator(tiffFilesDir)) {
            if (entry.path().extension() == ".tiff" || entry.path().extension() == ".tif") {
                std::string fileDir = entry.path();
                std::cout << entry.path().filename() << " \n" << std::endl;

                // save file in new folder
                saveAsGrayScale(fileDir, saveDir);
            }
        }
    } catch (const char *e) {
        std::cout << e << std::endl;
    }

}

cv::Mat Tiff2Ascii::tif_to_mat(const std::string &tiffDir) {

    if (!_isGDALinitialized) {
        // gdal initialization
        GDALAllRegister();
        GDALDriverManager::AutoLoadDrivers();
        _isGDALinitialized = true;
    };


    auto dataset = (GDALDataset*) GDALOpen(tiffDir.data(), GA_ReadOnly);
    int width = dataset->GetRasterXSize();
    int height = dataset->GetRasterYSize();

    float * inputdata = new float[width * height];

    dataset->RasterIO(GF_Read, 0, 0, width, height, inputdata, width, height, GDT_Float32, 1, nullptr, 0, 0, 0);
    GDALClose(dataset);

    cv::Mat  grayImg(height, width, 1);

    for (int y = 0; y < height ; y++){
        for(int x = 0; x< width; x++){
            grayImg.at<uchar>(y , x) = static_cast<uchar>(inputdata[y * width + x]);
        }
    }

    return grayImg;
}


