


#ifndef ASCII2TIFF_VICEVERSA_TIFF2ASCII_H
#define ASCII2TIFF_VICEVERSA_TIFF2ASCII_H


#include <iostream>
#include <vector>
#include <tiffio.h>
#include <tiff.h>
#include <string>
#include <bits/stdc++.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fts.h>

#include <gdal/gdal.h>
#include <gdal/gdal_priv.h>
#include <gdal/cpl_conv.h>
#include <opencv4/opencv2/core/core.hpp>
#include <opencv4/opencv2/imgproc//imgproc.hpp>
#include <opencv4/opencv2/imgcodecs/imgcodecs.hpp>
#include <opencv4/opencv2/highgui/highgui.hpp>



class Tiff2Ascii {

private:

    bool _isGDALinitialized = false;

    /// returning the file name with desired extension like(".asc" or ".tiff")
    std::string returnFileName(const std::string &fileAdd, const std::string &extension);

    /// save a tif as a normal image
    void saveAsGrayScale(const std::string &fileAdd, const std::string &saveDir);

public:

    Tiff2Ascii();

    ~Tiff2Ascii();

    /// change from TIFF into ASCII by address for one file (and if true save in in binary file directory)
    std::vector<std::vector<float>> Tif2Vector(const std::string &fileAdd, const bool &saveOrNot);

    /// change a TIFF file in a ASCII
    void changeAndSave_oneTiffFile(const std::string &fileAdd, const std::string &saveDir);

    /// change the TIFF files in a directory into ASCII
    void changeAndSave_MultipleTifFile(const std::string &tiffFilesDir, const std::string &dirName);

    /// change the TIFF files in a directory into normal image by opencv
    void tif_to_mat_multiple(const std::string &tiffFilesDir, const std::string &saveDir);

    /// return Tif Float as cv::Mat file
    cv::Mat tif_to_mat(const std::string &tiffDir);


};


#endif //ASCII2TIFF_VICEVERSA_TIFF2ASCII_H
