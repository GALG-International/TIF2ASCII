
/*
    created by Saleh (saleh.aghayaei@gmail.com)
    for testing the conversion form TIFF Float into ASCII ans normal image file
*/


#include <iostream>
#include <vector>

#include "../includes/TIFF2ASCII.h"



int main() {

    // initialization of object
    auto tiff2ascii = std::make_unique<Tiff2Ascii>();

    // file directories
    std::string tiffDir = "/media/saleh/LocalDrive/A/Projects/GALG/Task2/AnyInfo/Data/20220705_HotSpot-Test_EDBM/03_TIFF-Float/";
    std::string saveNormalImgDir = "../data/Exported_normalImg/";
    std::string saveAsciiDir = "../data/ExportedASCII/";
    std::string TifSample = "../data/00001.tif";


    // for returning a tiff file as vector<vector<float>> and save as ascii file
    std::vector<std::vector<float>> tiffFloat = tiff2ascii->Tif2Vector(TifSample, true);


    // saving a TIFF file as ascii in desired directory
    tiff2ascii->changeAndSave_oneTiffFile(TifSample, saveAsciiDir);


    // all Tif files in a directory into another directory as a ASCII file
    tiff2ascii->changeAndSave_MultipleTifFile(tiffDir, saveAsciiDir);

    //  all Tif files in a directory into another directory as a normal image in
    tiff2ascii->tif_to_mat_multiple(tiffDir, saveNormalImgDir);




    std::cout << "The End" << std::endl;
    return 0;

}
