


DESCRIPTION:
 
this is a package for converting the Tif to ASCII or normal image(normal image in case testing in the Metashape software)



INSTRUCTION:

1) first, you may need to install GDAL and OpenCV in your system.


2) In the src folder, there is a main.cpp file


3) four methods of the "tiff2ascii" object are provided in the main.cpp as an example


4) Tif2Vector for converting the Tif file into a vector of vector of float


5)  changeAndSave_oneTiffFile get one Tif file and save it as a ASCII file in the saveAsciiDir variable

6) in changeAndSave_MultipleTifFile you should introduce two directories, the first one containing all Tif files and the second for saving them into that directory


7) in tif_to_mat_multiple you should introduce two directories, the first one containing all Tif files and the second for saving them into that directory as normal image

